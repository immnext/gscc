<!DOCTYPE html>
<html lang="en">


<!-- Mirrored from eliteadmin.themedesigner.in/demos/bt4/inverse-php/pages-login.php by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 10 Jul 2019 13:17:27 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
@include('auth.head')

<body class="skin-default card-no-border">
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="loader">
            <div class="loader__figure"></div>
            <p class="loader__label">Elite admin</p>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <section id="wrapper" class="login-register" style="background-image:url(assets/images/background/login-register.jpg)">
        <div class="login-box card">
            <div class="card-body">
                <form class="form-horizontal form-material" id="loginform" method="POST" action="{{ route('login') }}">
                    @csrf
                    <a href="" class="text-center db"><img style="margin: 0; position: absolute; top: 10px; left: 50%; margin-right: -50%; transform: translate(-50%, -50%)" src="assets/images/logo.png" alt="Home" width="30%" /><br/> 
                    </a><br>
                    <p style="font-size: 20px; font-family: calibri; color: black" class="text-center">Welcome to the LW GSCC Portal</p>

                    <div class="form-group m-t-40">
                        <div class="col-xs-12">
                            <input style="font-family: calibri; font-size: 18px;" class="form-control" type="text" required="" placeholder="E-Mail" name="email">

                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-xs-12">
                            <input style="font-family: calibri; font-size: 18px;" class="form-control" type="password" required="" placeholder="Password" name="password">

                            @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-12">
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="customCheck1">
                                <label style="font-family: calibri; font-size: 18px;"class="custom-control-label" for="customCheck1">Remember me</label>
                                <a href="javascript:void(0)" id="to-recover" class="text-dark pull-right" style="font-family: calibri; font-size: 18px;"><i class="fa fa-lock m-r-5"></i> Forgot password?</a> 
                            </div>     
                        </div>
                    </div>
                    <div class="form-group text-center m-t-20">
                        <div class="col-xs-12">
                            <button style="font-family: calibri; font-size: 18px;" class="btn btn-info btn-lg btn-block text-uppercase btn-rounded" type="submit">Log In</button>
                        </div>
                    </div>
                    
                    <div class="form-group m-b-0">
                        <div class="col-sm-12 text-center" style="font-family: calibri; font-size: 18px;">
                            Don't have an account? <a href="/register" class="text-primary m-l-5"><b>Sign Up</b></a>
                        </div>
                    </div>
                </form>
                <form class="form-horizontal" id="recoverform" action="">
                    <div class="form-group ">
                        <div class="col-xs-12">
                            <h3>Recover Password</h3>
                            <p class="text-muted">Enter your Email and instructions will be sent to you! </p>
                        </div>
                    </div>
                    <div class="form-group ">
                        <div class="col-xs-12">
                            <input class="form-control" type="text" required="" placeholder="Email">
                        </div>
                    </div>
                    <div class="form-group text-center m-t-20">
                        <div class="col-xs-12">
                            <button class="btn btn-primary btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">Reset</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>
    
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
  @include('auth.script')
    
</body>


<!-- Mirrored from eliteadmin.themedesigner.in/demos/bt4/inverse-php/pages-login.php by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 10 Jul 2019 13:17:29 GMT -->
</html>