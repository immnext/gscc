<!DOCTYPE html>
<html lang="en">


<!-- Mirrored from eliteadmin.themedesigner.in/demos/bt4/inverse-php/pages-register.php by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 10 Jul 2019 13:17:30 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
@include('auth.head')

<body class="skin-default card-no-border">
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="loader">
            <div class="loader__figure"></div>
            <p class="loader__label">Elite admin</p>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <section id="wrapper">
        <div class="login-register" style="background-image:url(assets/images/background/login-register.jpg);">
            <div class="login-box card">
                <div class="card-body">
                    <div class="card-body">
                <form class="form-horizontal form-material" id="loginform" method="POST" action="{{ route('register') }}">
                    @csrf
                    <a href="" class="text-center db" ><img style="margin: 0; position: absolute; top: 10px; left: 50%; margin-right: -50%; transform: translate(-50%, -50%)" src="assets/images/logo.png" alt="Home" width="30%" /><br/> 
                    </a>
                    <p style="font-size: 20px; font-family: calibri; color: black" class="text-center">Welcome to the LW GSCC Portal</p>
                    <div class="form-group m-t-40">
                        <div class="form-group">
                            <div class="col-xs-12">
                                <input style="font-family: calibri; font-size: 18px;" class="form-control" type="text" required="" placeholder="Name" name="name" value="{{ old('name') }}">

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group ">
                            <div class="col-xs-12">
                                <input style="font-family: calibri; font-size: 18px;" class="form-control" type="email" required="" placeholder="Email" name="email" value="{{ old('email') }}">

                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        {{--<div class="form-group">--}}
                            {{--<div class="col-xs-12">--}}
                                {{--<input style="font-family: calibri; font-size: 18px;" class="form-control" type="text" required="" placeholder="Username" name="username" value="{{ old('username') }}">--}}

                                {{--@error('username')--}}
                                {{--<span class="invalid-feedback" role="alert">--}}
                                        {{--<strong>{{ $message }}</strong>--}}
                                    {{--</span>--}}
                                {{--@enderror--}}
                            {{--</div>--}}
                        {{--</div>--}}

                        <div class="form-group ">
                            <div class="col-xs-12">
                                <input style="font-family: calibri; font-size: 18px;" class="form-control" type="password" required="" placeholder="Password" name="password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-xs-12">
                                <input  style="font-family: calibri; font-size: 18px;" class="form-control" type="password" required="" placeholder="Confirm Password" name="password_confirmation">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-12">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="customCheck1">
                                    <label class="custom-control-label" for="customCheck1">I agree to all <a href="javascript:void(0)">Terms</a></label> 
                                </div> 
                            </div>
                        </div>
                        <div class="form-group text-center p-b-20" style="font-family: calibri; font-size: 16px;">
                            <div class="col-xs-12">
                                <button class="btn btn-info btn-lg btn-block btn-rounded text-uppercase waves-effect waves-light" type="submit">Sign Up</button>
                            </div>
                        </div>
                        <div class="form-group m-b-0">
                            <div class="col-sm-12 text-center" style="font-family: calibri; font-size: 18px;">
                                Already have an account? <a href="/login" class="text-info m-l-5"><br><b>Sign In</b></a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    @include('auth.script')
</body>


<!-- Mirrored from eliteadmin.themedesigner.in/demos/bt4/inverse-php/pages-register.php by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 10 Jul 2019 13:17:30 GMT -->
</html>