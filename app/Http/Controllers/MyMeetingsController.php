<?php

namespace App\Http\Controllers;

use App\Model\VgssMonthlyTrainingProgram;
use Illuminate\Http\Request;

class MyMeetingsController extends Controller
{
    public function vgssMonthlyTrainingProgram(Request $request){
        VgssMonthlyTrainingProgram::create($request->all());
        return view('');
    }


    public function showMonthlyTrainingProgram(VgssMonthlyTrainingProgram $monthlyTrainingProgram){
        // View VGSS Monthly Training Programs List
        return view('', compact('monthlyTrainingProgram'));
    }


}
