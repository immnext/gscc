<?php

namespace App\Http\Controllers;

use App\Model\InformationCenter;
use Illuminate\Http\Request;

class InformationCenterController extends Controller
{
    public function viewInfo(InformationCenter $informationCenter){
        return view('InfoCenter.Posts', compact('informationCenter'));
    }


    public function createInformationCenter(Request $request){
        InformationCenter::create($request->all());
        return view('InfoCenter.create');
    }
}
