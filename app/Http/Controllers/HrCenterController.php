<?php

namespace App\Http\Controllers;

use App\Model\EndOfTenureReport;
use App\Model\EndOfTenureAppraisal;
use App\Model\MonthlyAchievementReport;
use App\Model\QuarterlyAppraisal;
use App\Model\TestsFromTrainingPrograms;
use Illuminate\Http\Request;

class HrCenterController extends Controller
{
    /**Monthly Achievement Methods starts Here **/
    public function submitMonthlyAchievementReport(Request $request){
        MonthlyAchievementReport::create($request->all());
        return view('HrCenter.create_smar');
    }

    public function showMonthlyAchievementReport(MonthlyAchievementReport $monthlyAchievementReport){
        return view('', compact('monthlyAchievementReport'));
    }

    /**Monthly Achievement Methods ends Here **/





    /**End of Tenure Methods starts Here **/

    public function submitEndOfTenureReport(Request $request){
        EndOfTenureReport::create($request->all());
        return view('HrCenter.create_setr');
    }


    public function showEndOfTenureReport(EndOfTenureReport $endOfTenureReport){
        return view('', compact('endOfTenureReport'));
    }

    /**End of Tenure Methods ends Here **/




    /**Quarterly Appraisal Methods starts Here **/

    public function submitQuarterlyAppraisal(Request $request){
        QuarterlyAppraisal::create($request->all());
        return view('HrCenter.create_sqa');
    }

    public function showQuarterlyAppraisal(QuarterlyAppraisal $quarterlyAppraisal){
        return view('', compact('quarterlyAppraisal'));
    }

    /**Quarterly Appraisal Methods ends Here **/






    /**End Tenure Appraisal Methods starts Here **/

    public function submitEndTenureAppraisal(Request $request){
        EndOfTenureAppraisal::create($request->all());
        return view('HrCenter.create_seta');
    }

    public function showEndTenureAppraisal(EndOfTenureAppraisal $endTenureAppraisal){
        return view('', compact('endTenureAppraisal'));
    }

    /**End Tenure Appraisal Methods ends Here **/






    public function submitTestsFromTrainingPrograms(Request $request){
        TestsFromTrainingPrograms::create($request->all());
        return view('HrCenter.create_stftp');
    }


}
