<?php

namespace App\Http\Controllers;

use App\Model\ChildInformation;
use App\Model\EducationalQualification;
use App\Model\MedicalInformation;
use App\Model\ProfessionalQualification;
use App\Model\BlwFellowshipInvolvementDetail;
use App\Model\Profile;
use App\Model\SpousalInformation;
use App\User;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

class MyProfileController extends Controller
{

    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {

    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function BlwFellowshipInvolvementDetail(BlwFellowshipInvolvementDetail $blwFind ){
        // The blwFind means blw_fellowship Involvement details
        return view('MyProfile.blw_find', compact('blwFind'));
    }


    public function addEducationalQualification(Request $request){
        EducationalQualification::create($request->all());
        return view('EducationalQualification.create_eq');
    }


    public function addProfessionalQualification(Request $request){
        ProfessionalQualification::create($request->all());
        return view('EducationalQualification.create_ep');
    }


    public function addSpousalInfo(Request $request){
        SpousalInformation::create($request->all());
        return view('FamilyInformation.addSpousalInfo');
    }


    public function addChildInfo(Request $request){
        ChildInformation::create($request->all());
        return view('FamilyInformation.addChildInfo');
    }


    public function viewChildrenInfo(ChildInformation $childInformation){
        return view('FamilyInformation.viewChildrenInfo', compact('childInformation'));
    }


    public function addMedicalInformation(Request $request){
        MedicalInformation::create($request->all());
        return view('MedicalInformation.addMedicalInfo');
    }


    public function showProfile(User $user){
        return view('MyProfile.profile', compact('user'));
    }


    public function editProfile(User $user){
        $this->authorize('update', $user->profile);
        return view('MyProfile.profile', compact('user'));
    }


    public function updateProfile(User $user){
        $this->authorize('update', $user->profile());

        $data = \request()->validate([
            'title' => 'required',
            'first_name' => 'required',
            'last_name' => 'required',
            'other_name' => ''
        ]);

        auth()->user()->update($data);
        return view('MyProfile.profile');
    }


    public function updateImage(User $user){
        $this->authorize('update', $user->profile());

        if(\request('image')){
            $imagePath = \request('image')->store('profile', 'public');
            $image = Image::make(public_path("storage/{$imagePath}"))->fit(1000, 1000);
            $image->save();

            $imageArray = ['image' => $imagePath];
        }

        auth()->user()->update( $imageArray ?? []);
        return view('MyProfile.profile');


    }





}



