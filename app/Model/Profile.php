<?php

namespace App\Model;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $guarded = [];

    public function user(){
        return $this->belongsTo(User::class);
    }


    public function profileImage()
    {
        $imagePath = ($this->image) ? $this->image : 'profile/no_image.jpg';
        return '/storage/'. $imagePath;
    }
}
